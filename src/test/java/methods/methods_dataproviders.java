package methods;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Set;

import org.apache.poi.hpsf.Array;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;

public class methods_dataproviders {
	static String userDirectory = System.getProperty("user.dir");
	
	  public static Object[][] readJSON(String filePath,String objectname,String[] parameters_) throws IOException, ParseException {
		  JSONParser parser = new JSONParser();
		  JSONObject dataset_ = null;

		  Reader reader = new FileReader(filePath);
		  JSONObject jsonObject = (JSONObject) parser.parse(reader);
		 
		  
		  JSONArray datesobject= (JSONArray) jsonObject.get(objectname);
		  int size_items = datesobject.size();
		 int size_para = parameters_.length;
		  
		  Object[][] data = new Object[size_items][size_para];

		  
		  
		  for(int i=0;i<size_items;i++) {
			  dataset_ = (JSONObject) datesobject.get(i);
			for(int j=0;j<size_para;j++) {
				String paraname = parameters_[j];
		  String value = dataset_.get(paraname).toString();
		  data[i][j] = value;
			}
		  }
             
			return data;
            
	  } 
	  
	  @DataProvider (name = "data-provider") 
	  public static Object[][] dpMethod() throws Exception, Exception{ 
		  
		  String[] para = { "date","api_key"};
		  return readJSON(userDirectory+"\\jsonDataFiles\\datafile.json","dates",para); 
		  }
}
