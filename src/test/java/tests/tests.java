package tests;

import java.util.List;

import org.testng.ISuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import io.restassured.RestAssured;
import io.restassured.response.ResponseBody;
import methods.methods_dataproviders;

public class tests {
	
	static String userDirectory = System.getProperty("user.dir");
	@Test (dataProvider = "data-provider" , dataProviderClass = methods_dataproviders.class) 
	public void MARS_weather(String date_,String apikey){ 
		
		
		RestAssured.baseURI = "https://api.nasa.gov/planetary/apod";
		ResponseBody request = RestAssured.given()
			  .param("api_key",apikey) 
			  .and()
			  .param("date", date_) 
			  .when()
			  .get()
			  .body();
			  System.out.print(request.asString());
			  
			 
	}
	
	
	  
	 
	  
}
